<?php
defined('BASE_PATH') || define('BASE_PATH', getenv('BASE_PATH') ?: realpath(dirname(__FILE__) . '/../..'));
require BASE_PATH . '/vendor/autoload.php';
require_once('mail.php');

use Phalcon\Http\Response;
use Phalcon\Http\Request;
use Phalcon\Filter;
use Firebase\JWT\JWT;
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;
use PHPMailer\PHPMailer\SMTP;

class APIController extends \Phalcon\Mvc\Controller
{
    /**
     * Simple GET API Request
     * 
     * @method GET
     * @link /api/get
     */


    public function indexAction()
    {
        $key = "example_key";
        $payload = array(
            "iss" => "http://example.org",
            "aud" => "http://example.com",
            "iat" => 1356999524,
            "nbf" => 1357000000
        );
        $jwt = JWT::encode($payload, $key);
        $decoded = JWT::decode($jwt, $key, array('HS256'));
        
        // Disable View File Content
        $this->view->disable();

        // Getting a response instance
        // https://docs.phalcon.io/3.4/en/response.html
        $response = new Response();
        // $response->setHeader('Cache-Control', 'max-age=86400');

        // Getting a request instance
        // https://docs.phalcon.io/3.4/en/request
        $request = new Request();

        if ($request->isGet()){
            $returnData = [
                "Status" => "Server is Healthy"
            ];

            //Set Status Code
            $response->setStatusCode(200, 'OK');

            // Set content of the response
            $response->setJsonContent([
                "status" => true,
                "error" => false,
                "app-version" => phpversion(),
                "phalcon-version" => "3.4",
                "data" => $returnData
            ]);
        } else {

            // Set status code
            $response->setStatusCode(405, 'Method Not Allowed');

            // Set the content of the response
            $response->setJsonContent(["status" => false, "error" => "Method Not Allowed"]);

        }

        // Send response to the client
        $response->send();
    }

    public function decodeAction($jwt){
        $this->view->disable();
        $response = new Response();
        $request = new Request();

        if ($request->isPost()) {
            $returnData = [
                "name" => $paramData->name,
                "email" => $paramData->email,
                "paramData" => $paramData
            ];
            $response->setStatusCode(200, 'OK');
            $response->setJsonContent(["status" => true, "error" => false, "data" => $returnData ]);

        } else {
            $response->setStatusCode(405, 'Method Not Allowed');
            $response->setJsonContent(["status" => false, "error" => "Method Not Allowed"]);
        }
        $response->send();
    }

    public function signupAction(){
        $this->view->disable();
        $response = new Response();
        $request = new Request();
        $filter = new Filter();

        $expiredTime = time() + (3*60);

        if ($request->isPost()) {
            // $email = $filter->sanitize($_POST['email'], 'email');
            $email = $filter->sanitize($request->getPost('email'), 'email');
            

            // Check is empty Email and Password
            if(empty($_POST['email']) == true){
                $response->setStatusCode(400);
                $response->setJsonContent(["success" => false,"error" => true, "message" => "Email can not be empty"]);

                return $response;
            }

            if(empty($_POST['password']) == true){
                $response->setStatusCode(400);
                $response->setJsonContent(["success" => false,"error" => true, "message" => "Password can not be empty"]);

                return $response;
            }
            
            // if(empty())
            if(filter_var($_POST['email'], FILTER_VALIDATE_EMAIL)){
                echo("$email is valid email address");
            } else {
                echo("$email is not valid email address");
                return;
            }

            // Check and validate password


            $payload = [
                "exp" => $expiredTime,
                "name" => "jamaludinsalam",
                "email" => $email,
                "password" => "1234546"
            ];
            $key = "example_key";
            $jwt = JWT::encode($payload, $key);
            
            // Function for sending token to email
            // sendMail($jwt);

            $response->setStatusCode(200, 'OK');
            $response->setJsonContent(["status" => true, "error" => false, "data" => $payload ]);

        } else {
            $response->setStatusCode(405, 'Method Not Allowed');
            $response->setJsonContent(["status" => false, "error" => "Method Not Allowed"]);
        }
        $response->send();

    }

    public function activateAction($token){
        $this->view->disable();
        $response = new Response();
        $request = new Request();

        
        // if ($request->isPost()) {
        //     $response->setStatusCode(200, 'OK');
        //     $response->setJsonContent(["status" => true, "error" => false, "data" => $decoded ]);

        // } else {
        //     $response->setStatusCode(401, 'Method Not Allowed');
        //     $response->setJsonContent(["status" => false, "error" => "Signature verification failed"]);
        // }

        try{
            $key = "example_key";
            $decoded = JWT::decode($token, $key, array('HS256'));
            $response->setStatusCode(200, 'OK');
            $response->setJsonContent(["status" => true, "error" => false, "data" => $decoded ]);
            $response->send();
        } catch (\Exception $e) {
            $response->setStatusCode(401, 'Token invalid');
            $response->setJsonContent(["status" => false, "error" => $e->getMessage()]);
            $response->send();
        }
    }

}

